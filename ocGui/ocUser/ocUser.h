/**********************************************
 * @author：Jun
 * @time  ：2018/8/1
 * @brief ：实现具体功能
 * @note  ：实现GUI与用户层交互
**********************************************/
#ifndef _OCUSER_H_
#define _OCUSER_H_

#include "ocConfig.h"

//屏幕刷写方向
#define HIGH_LOW_LEFT_RIGHT 0//从上到下从左到右
#define HIGH_LOW_RIGHT_LEFT 1//从上到下从右到左
#define LOW_HIGH_LEFT_RIGHT 2//从下到上从左到右
#define LOW_HIGH_RIGHT_LEFT 3//从下到上从右到左
#define LEFT_RIGHT_HIGH_LOW 4//从左到右从上到下
#define LEFT_RIGHT_LOW_HIGH 5//从左到右从下到上
#define RIGHT_LEFT_HIGH_LOW 6//从右到左从上到下
#define RIGHT_LEFT_LOW_HIGH 7//从右到左从下到上

#define OCCLEAR_COLOR 0

//打点函数包含颜色信息
#define INCCOLORINFO 1

//用户配置信息
typedef struct
{
	uint8_t direction;
	uint8_t fastDraw;
}ocUser_t;

/**********************************************
 * @brief ：GUI初始化
 * @param : none
 * @note  ：
 * @retval: 初始化成功返回oc_OK，失败返回oc_ON
**********************************************/
uint8_t ocGuiInit(void);

/**********************************************
 * @brief ：在指定的位置绘制一个像素点
 * @param : Xpos: x坐标
 * @param : Ypos: y坐标
 * @note  ：初始化成功返回oc_OK，失败返回oc_ON
 * @retval: 绘制成功返回oc_OK，失败返回oc_ON
**********************************************/
#if INCCOLORINFO
uint8_t ocPutDot(uint16_t Xpos, uint16_t Ypos, uint32_t color);
#else
uint8_t ocPutDot(uint16_t Xpos, uint16_t Ypos);
#endif

/**********************************************
 * @brief ：设置字体颜色
 * @param : Color: 颜色
 * @note  ：none
 * @retval: 绘制成功返回oc_OK，失败返回oc_ON
**********************************************/
uint8_t ocSetColor(uint32_t Color);

#endif

