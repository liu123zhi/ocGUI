/**********************************************
	* @author: Jun
	* @time  : 2018/8/1
	* @brief ：实现具体功能
	* @note  ：实现GUI与用户层交互
**********************************************/
#include "ocUser.h"

#include "./lcd/bsp_lcd.h" 

ocUser_t ocUser;

static uint8_t ocSetInit(void);

/**********************************************
 * @brief ：接口初始化
 * @param : none
 * @note  ：
 * @retval: 初始化成功返回oc_OK，失败返回oc_ON
**********************************************/
uint8_t ocGuiInit(void)
{
	/* 初始化LCD */
	LCD_Init(LCD_INTERRUPT_DISABLE);
	
	ocSetInit();
	return oc_OK;
}

/**********************************************
 * @brief ：GUI基础设置初始化
 * @param : none
 * @note  ：
 * @retval: 初始化成功返回oc_OK，失败返回oc_ON
**********************************************/
static uint8_t ocSetInit(void)
{
	//用户配置信息
	ocUser.direction = HIGH_LOW_LEFT_RIGHT;
	ocUser.fastDraw = oc_Disable;
	(void)ocUser;
	
	//字体设置
	ocFontSet.fontForm = acs24_12;
	ocFontSet.textColor = 0x00ffff;
	(void)ocFontSet;
	
	ocWave.disAxis = oc_Enable;
	ocWave.waveMaxData = 200;
	ocWave.updatePos = coMemorySize - 20;
	ocWave.arrowSize = 5;
	
	return oc_OK;
}

/**********************************************
 * @brief ：设置字体颜色
 * @param : Color: 颜色
 * @note  ：none
 * @retval: 绘制成功返回oc_OK，失败返回oc_ON
**********************************************/
uint8_t ocSetColor(uint32_t Color)
{
	LCD_SetTextColor(Color);
	return oc_OK;
}

/**********************************************
 * @brief ：在指定的位置绘制一个像素点
 * @param : Xpos: x坐标
 * @param : Ypos: y坐标
 * @note  ：none
 * @retval: 绘制成功返回oc_OK，失败返回oc_ON
**********************************************/
#if INCCOLORINFO
uint8_t ocPutDot(uint16_t Xpos, uint16_t Ypos, uint32_t color)
#else
uint8_t ocPutDot(uint16_t Xpos, uint16_t Ypos)
#endif
{
	ocSetColor(color);
	PutPixel(Xpos,Ypos);
	return oc_OK;
}

/**********************************************
 * @brief ：输出整个屏幕的数据
 * @note  ：方便快速显示
 * @retval: 绘制成功返回oc_OK，失败返回oc_ON
**********************************************/
uint8_t ocDrawFull(void)
{
	
	return oc_OK;
}

/**********************************************
 * @brief ：以当前背景颜色清除整个屏幕
 * @param : Color: 颜色
 * @note  ：none
 * @retval: 绘制成功返回oc_OK，失败返回oc_ON
**********************************************/
uint8_t ocClear(uint32_t Color)
{
	LCD_Clear(Color);
	return oc_OK;
}

/**********************************************
 * @brief ：背光控制
 * @param : onOff: 开关背光 1：打开  0：关闭
 * @note  ：none
 * @retval: 绘制成功返回oc_OK，失败返回oc_ON
**********************************************/
uint8_t ocBackLight(uint32_t onOff)
{
	
	return oc_OK;
}

